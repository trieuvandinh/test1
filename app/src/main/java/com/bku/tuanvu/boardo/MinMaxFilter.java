package com.bku.tuanvu.boardo;


/**
 * Created by Trieu on 15-Mar-18.
 */

import android.text.InputFilter;
import android.text.Spanned;

class MinMaxFilter implements InputFilter {

    private int min, max;

    public MinMaxFilter(int min, int max) {
        this.min = min;
        this.max = max;
    }

    public MinMaxFilter(String min, String max) {
        this.min = Integer.parseInt(min);
        this.max = Integer.parseInt(max);
    }

    @Override
    public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
        try {
            int input = Integer.parseInt( source.toString());
            if (isInRange(min, max, input))
                return null;
        } catch (NumberFormatException nfe) { }
        return "";
    }

    private boolean isInRange(int a, int b, int c) {
        return b > a ? c >= a && c <= b : c >= b && c <= a;
    }
}