package com.bku.tuanvu.boardo;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.InputFilter;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

/**
 * Created by Trieu on 16-Mar-18.
 */

public class AddSubjectActivity extends AppCompatActivity{
    private Spinner spnCategory;
    private EditText period1,period2;

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_subject);
        // spinner chọn ngày
        spnCategory = findViewById(R.id.setDay);
        String[] day = getResources().getStringArray(R.array.listDays);
        ArrayAdapter adapter = new ArrayAdapter(this, android.R.layout.simple_spinner_item,day);
        adapter.setDropDownViewResource(android.R.layout.simple_list_item_single_choice);
        spnCategory.setAdapter(adapter);
        spnCategory.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                Toast.makeText(AddSubjectActivity.this, spnCategory.getSelectedItem().toString(), Toast.LENGTH_SHORT).show();
            }
            @Override
            public void onNothingSelected(AdapterView<?> adapterView){}
        });
        // set min max input tiết học
        period1= findViewById(R.id.firstPeriod);
        period2= findViewById(R.id.lastPeriod);
        period1.setFilters(new InputFilter[]{new MinMaxFilter(1,12)});
        period2.setFilters(new InputFilter[]{new MinMaxFilter(1,12)});


    }
}
