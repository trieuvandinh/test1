package com.bku.tuanvu.boardo;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toolbar;

import java.util.Calendar;
import java.util.GregorianCalendar;

public class MainActivity extends AppCompatActivity {
    private String mainTittle;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //tạo sự kiện cho CreateButton
        final ImageButton CreateBtn = findViewById(R.id.CreateButton);
        CreateBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, AddSubjectActivity.class);
                startActivity(intent);
            }
        });
        //get the current week of the year
        int weekOfYear = new GregorianCalendar().get(Calendar.WEEK_OF_YEAR);
        //set tittle
        mainTittle= (String) getResources().getText(R.string.week)+" "+ weekOfYear;
        setTitle(mainTittle);
    }
}
